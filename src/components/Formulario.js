import React from 'react';
import styles from './Formulario.module.css';
import useSelect from '../hooks/useSelect'; // importamos el custon hook
import PropTypes from 'prop-types';

const Formulario = ({guardarCategoria}) => {

    // opciones que vendran desde la api
    const OPCIONES = [
        {value: 'general', label: 'General'},
        {value: 'business', label: 'Negocios'},
        {value: 'entertainment', label: 'Entretenimiento'},
        {value: 'health', label: 'Salud'},
        {value: 'science', label: 'Ciencias'},
        {value: 'sports', label: 'Deportes'},
        {value: 'technology', label: 'Tecnologia'}
    ]

    //utilizamos el custon hook
    // como valor inicial (pasamos genral "para que se vea las noticias de genral antes de que busque el cliente")
    const [categoria, SelectNoticias] = useSelect('general',OPCIONES);

    // cuando el usuario le de submit al form, pasaremos la categotia a app.js
    const buscarNoticias = (e) => {
        e.preventDefault();

        // de esta manera enviaremos informacion al padre
        guardarCategoria(categoria); // categoria >> lleva ( tiene el acceso a custon hook)
    }
    
    return ( 
        <div className={`${styles.buscador} row`}>
            <div className="col s12 m8 offset-m2">
                <form
                    onSubmit={buscarNoticias}
                >
                    <h2 className={styles.heading}> Noticias por categoria </h2>
                    <SelectNoticias />
                    <div className="input-field col s12">
                        <input
                            type="submit"
                            className={`${styles.btn_block} btn-large amber darken-2`}
                            value="Buscar"
                        />
                    </div>
                </form>
            </div>
        </div>
     );
}

// documentacion
Formulario.propTypes = {
    guardarCategoria: PropTypes.func.isRequired
}
 
export default Formulario;