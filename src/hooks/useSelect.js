import React, { useState } from 'react';

const useSelect = (stateInicial, opciones) => {

    //state del custon hook
    const [state, actualizarState] = useState(stateInicial); // pasamos vacio el srtate inicial
    
    const SelectNoticia = () => (
        
        <select
            className="browser-default"
            value={state}
            onChange={ e => actualizarState(e.target.value)}
        >
            {opciones.map(opcion => (
                <option key={opcion.value} value={opcion.value}>{opcion.label}</option>
           ))} 
        </select>

    );

    return [state, SelectNoticia]; // retornamos state(lo que el usuario seleccione ) y selectNoticia ( lo que se imprimira)
}
 
export default useSelect;