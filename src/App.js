import React, { Fragment, useState, useEffect } from 'react';
import Header from './components/Header'
import Formulario from './components/Formulario';
import ListadoNoticias from './components/ListadoNoticias';

function App() {

  // pasaremos el resultado de formulario hacia otro componente
  // definir la categoria y noticias
  const [categoria, guardarCategoria] = useState(''); // hasta que el usuario de submit el formulario estara vacio
  const [noticias, guardarNoticias] = useState([]); // se llenar el arreglo de noticias extraidas del api

  // cuando cambie .. traera la informacion
  useEffect(() => {
    const consultarAPI = async () => {
      const url = `http://newsapi.org/v2/top-headlines?country=ar&category=${categoria}&apiKey=d71d4f091c594e13b7b303275cbebb85`;
      
      const respuesta = await fetch(url);
      const noticias = await respuesta.json();

      guardarNoticias(noticias.articles); // aniadiendo las noticias al arreglo de noticias
      
    }
    consultarAPI();
  },[categoria]);

  return (
    <Fragment>
      <Header
        titulo='Buscador de Noticias'
      />
      <div className="container white">
        <Formulario
          guardarCategoria={guardarCategoria}
        />
        <ListadoNoticias
          noticias ={noticias}
        />
      </div>
    </Fragment>
  );
}

export default App;
